<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');



// DaySheet Routes
Route::resource('/daysheets','DaySheetController');



// ExpenseTransaction Routes
Route::resource('/expense_transaction','ExpenseTransactionController');

Route::prefix('admin')->group(function () {
        Route::resource('/chem_types', 'Admin\ChemTypesController');
        Route::resource('/expense_category','Admin\ExpenseCategoryController');
        Route::resource('/chem_sale_price','Admin\ChemSalePriceController');
});

Route::prefix('accounts')->group(function () {
      Route::resource('/daily_profit','Accounts\DailyProfitController');
});

Route::prefix('transactions')->group(function () {
      Route::resource('/purchase_invoices','Transactions\PurchaseInvoiceController');
});
