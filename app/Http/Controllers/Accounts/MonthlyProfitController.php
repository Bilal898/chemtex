<?php

namespace App\Http\Controllers;

use App\MonthlyProfit;
use Illuminate\Http\Request;

class MonthlyProfitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MonthlyProfit  $monthlyProfit
     * @return \Illuminate\Http\Response
     */
    public function show(MonthlyProfit $monthlyProfit)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MonthlyProfit  $monthlyProfit
     * @return \Illuminate\Http\Response
     */
    public function edit(MonthlyProfit $monthlyProfit)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MonthlyProfit  $monthlyProfit
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MonthlyProfit $monthlyProfit)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MonthlyProfit  $monthlyProfit
     * @return \Illuminate\Http\Response
     */
    public function destroy(MonthlyProfit $monthlyProfit)
    {
        //
    }
}
