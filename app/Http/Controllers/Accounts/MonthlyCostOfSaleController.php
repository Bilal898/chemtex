<?php

namespace App\Http\Controllers;

use App\MonthlyCostOfSale;
use Illuminate\Http\Request;

class MonthlyCostOfSaleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MonthlyCostOfSale  $monthlyCostOfSale
     * @return \Illuminate\Http\Response
     */
    public function show(MonthlyCostOfSale $monthlyCostOfSale)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MonthlyCostOfSale  $monthlyCostOfSale
     * @return \Illuminate\Http\Response
     */
    public function edit(MonthlyCostOfSale $monthlyCostOfSale)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MonthlyCostOfSale  $monthlyCostOfSale
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MonthlyCostOfSale $monthlyCostOfSale)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MonthlyCostOfSale  $monthlyCostOfSale
     * @return \Illuminate\Http\Response
     */
    public function destroy(MonthlyCostOfSale $monthlyCostOfSale)
    {
        //
    }
}
