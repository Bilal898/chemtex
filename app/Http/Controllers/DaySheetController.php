<?php

namespace App\Http\Controllers;

use App\Models\Admin\ChemType;
use App\Models\Admin\ExpenseCategory;
use App\Models\DaySheet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\User;

class DaySheetController extends Controller
{
    public function __construct()
    {
       $this->middleware('auth');
    }

    public function index()
    {
      $daySheetCount = day_sheet_count();

      $daySheetColl = create_daysheet_array();

      $lastDaySheetID = last_day_sheet_id();

      $userChemNameArray = create_chem_type_name_array();
      $chemTypeCount = chem_type_count();

        return view('daysheets.index', [
                'daySheetCount' => $daySheetCount,
                'day_sheets' => $daySheetColl,
                'daysheetCount' => $daySheetCount,
                'lastDaySheetID' => $lastDaySheetID,
                'userChemNameArray' => $userChemNameArray,
                'chemTypeCount' => $chemTypeCount,
              ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

      $userChemTypeCount  = chem_type_count();
      if($userChemTypeCount == 0){
        return view('home');
      }      
      /////////////////Expenses Drop down
            $catCount = DB::table('expense_categories')->where('user_id', Auth::id())
                            ->count();
            $userexpCatColl = ExpenseCategory::where('user_id', Auth::id())
                                        ->orderBy('id', 'desc')->get();
              if(!empty($expCatCount)){
            $userExpenseCategories = create_expense_category_array($userexpCatColl);
                  }   else {
                    $userExpenseCategories = array_fill(0, 10, null);
                  }



      $userChemTypeStockCount  = chem_type_stock_count();
      $userChemTypeNameArray = create_chem_type_name_array();
      $userChemTypeStockArray = create_chem_type_stock_array();


        return view('daysheets.create', [
                'userChemTypeNameArray' => $userChemTypeNameArray,
                'userChemTypeStockArray' => $userChemTypeStockArray,
                'userChemTypeCount' => $userChemTypeCount,
                'userChemTypeStockCount' => $userChemTypeStockCount,
                'catCount' => $catCount,
                'categories' => $userExpenseCategories,
              ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request, [
          'chem_type_sale.*' => 'required|numeric',
          'ds_date' => 'required',
      ]);



      $newDaySheet = create_daysheet_db($request->ds_date);

      $reqSaleArray = create_req_array($request->chem_type_sale);
      $newSales = create_sales_db($reqSaleArray, $newDaySheet);
      create_chem_type_foreignKey($newDaySheet);

      $reqStockArray = create_req_array($request->chem_type_stock);
      $newStocks = create_stocks_db($reqStockArray, $newDaySheet);

      return redirect('daysheets')->with('status', 'Day Sheet created!');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DaySheet  $daySheet
     * @return \Illuminate\Http\Response
     */
    public function show(DaySheet $daySheet)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DaySheet  $daySheet
     * @return \Illuminate\Http\Response
     */
    public function edit(DaySheet $daySheet)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DaySheet  $daySheet
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DaySheet $daySheet)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DaySheet  $daySheet
     * @return \Illuminate\Http\Response
     */
    public function destroy(DaySheet $daySheet)
    {
        //
    }
}
