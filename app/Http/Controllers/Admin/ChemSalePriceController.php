<?php

namespace App\Http\Controllers\Admin;

use App\ChemSalePrice;
use Illuminate\Http\Request;

class ChemSalePriceController extends \App\Http\Controllers\Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.chem_sale_price.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ChemSalePrice  $chemSalePrice
     * @return \Illuminate\Http\Response
     */
    public function show(ChemSalePrice $chemSalePrice)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ChemSalePrice  $chemSalePrice
     * @return \Illuminate\Http\Response
     */
    public function edit(ChemSalePrice $chemSalePrice)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ChemSalePrice  $chemSalePrice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ChemSalePrice $chemSalePrice)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ChemSalePrice  $chemSalePrice
     * @return \Illuminate\Http\Response
     */
    public function destroy(ChemSalePrice $chemSalePrice)
    {
        //
    }
}
