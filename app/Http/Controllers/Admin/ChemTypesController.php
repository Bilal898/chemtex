<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin\ChemType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\User;

class ChemTypesController extends \App\Http\Controllers\Controller
{
    public function __construct()
    {
         $this->middleware('auth');
     }


    public function index()
    {
      $userChemTypesCount = chem_type_count();


      $userChemTypeArray = create_chem_type_array();

      return view('admin.chem_types.index', [
              'userChemTypesCount' => $userChemTypesCount,
              'userChemTypeArray' => $userChemTypeArray,

            ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request, [
        'chem_type_name.0' => 'required_without_all:chem_type_name.*',
      ]);

      $result = create_chem_type_db($request->chem_type_name);
      return redirect('admin/chem_types')->with('status', 'Chemical Types created!');


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ChemType  $chemType
     * @return \Illuminate\Http\Response
     */
    public function show(ChemType $chemType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ChemType  $chemType
     * @return \Illuminate\Http\Response
     */
    public function edit(ChemType $chemType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ChemType  $chemType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ChemType $chemType)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ChemType  $chemType
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $chemTypeFirst = DB::table('chem_types')->where([
                              ['user_id', '=', Auth::id()],
                              ['id', '=', $id]  ])->first();

      if($chemTypeFirst->day_sheet_id != null) {
        return redirect('admin/chem_types')
            ->with('status', 'Chemical Types cannot be deleted, there are sales attached!');
          }



        DB::table('chem_types')->where([
                ['user_id', '=', Auth::id()],
                ['id', '=', $id],
                            ])->delete();
      return redirect('admin/chem_types')
              ->with('status', 'Chemical Type deleted!');
    }
}
