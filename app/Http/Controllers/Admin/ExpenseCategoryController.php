<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin\ExpenseCategory;
use App\Models\Admin\ChemType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\User;

class ExpenseCategoryController extends \App\Http\Controllers\Controller
{

    public function __construct()
    {
         $this->middleware('auth');

     }

    public function index()
    {
      $userExpCatCount = expense_category_count();


      $userExpCatArray = create_expense_category_array();

      return view('admin.expense_category.index', [
              'userExpCatCount' => $userExpCatCount,
              'userExpCatArray' => $userExpCatArray,

            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request, [
      'exp_cat_name.0' => 'required_without_all:exp_cat_name.*',

      ]);

      $result = create_expense_category_db($request->exp_cat_name);

            return redirect('admin/expense_category')->with('status', 'Category created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ExpenseCategory  $expenseCategory
     * @return \Illuminate\Http\Response
     */
    public function show(ExpenseCategory $expenseCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ExpenseCategory  $expenseCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(ExpenseCategory $expenseCategory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ExpenseCategory  $expenseCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ExpenseCategory $expenseCategory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ExpenseCategory  $expenseCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(ExpenseCategory $expenseCategory)
    {
        //
    }
}
