<?php

namespace App\Http\Controllers\Transactions;

use App\DailyStock;
use Illuminate\Http\Request;

class DailyStockController extends \App\Http\Controllers\Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DailyStock  $dailyStock
     * @return \Illuminate\Http\Response
     */
    public function show(DailyStock $dailyStock)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DailyStock  $dailyStock
     * @return \Illuminate\Http\Response
     */
    public function edit(DailyStock $dailyStock)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DailyStock  $dailyStock
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DailyStock $dailyStock)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DailyStock  $dailyStock
     * @return \Illuminate\Http\Response
     */
    public function destroy(DailyStock $dailyStock)
    {
        //
    }
}
