<?php

namespace App\Http\Controllers;

use App\DailyExpenseTransaction;
use Illuminate\Http\Request;

class DailyExpenseTransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DailyExpenseTransaction  $dailyExpenseTransaction
     * @return \Illuminate\Http\Response
     */
    public function show(DailyExpenseTransaction $dailyExpenseTransaction)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DailyExpenseTransaction  $dailyExpenseTransaction
     * @return \Illuminate\Http\Response
     */
    public function edit(DailyExpenseTransaction $dailyExpenseTransaction)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DailyExpenseTransaction  $dailyExpenseTransaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DailyExpenseTransaction $dailyExpenseTransaction)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DailyExpenseTransaction  $dailyExpenseTransaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(DailyExpenseTransaction $dailyExpenseTransaction)
    {
        //
    }
}
