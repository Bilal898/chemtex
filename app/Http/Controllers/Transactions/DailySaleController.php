<?php

namespace App\Http\Controllers\Transactions;

use App\DailySale;
use Illuminate\Http\Request;

class DailySaleController extends \App\Http\Controllers\Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DailySale  $dailySale
     * @return \Illuminate\Http\Response
     */
    public function show(DailySale $dailySale)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DailySale  $dailySale
     * @return \Illuminate\Http\Response
     */
    public function edit(DailySale $dailySale)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DailySale  $dailySale
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DailySale $dailySale)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DailySale  $dailySale
     * @return \Illuminate\Http\Response
     */
    public function destroy(DailySale $dailySale)
    {
        //
    }
}
