<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DaySheet extends Model
{
    protected $fillable = [
         'daysheet_date',
        'daily_sales_total', 'daily_revenue_total', 
        'daily_expenses_total',
        ];

    public function ds_dailysales()
    {
        return $this->hasOne('App\Models\Transaction\DailySale' );
    }
    public function ds_dailystocks()
    {
        return $this->hasOne('App\Models\Transaction\DailyStock' );
    }
    public function ds_chemtypes()
    {
        return $this->hasMany('App\Models\Admin\ChemType' );
    }
}
