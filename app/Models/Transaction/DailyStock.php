<?php

namespace App\Models\Transaction;

use Illuminate\Database\Eloquent\Model;

class DailyStock extends Model
{
  protected $fillable = [


  'chem_type1_stock','chem_type2_stock', 'chem_type3_stock',
  'chem_type4_stock', 'chem_type5_stock',

    ];

    public function dailystocks_user()
    {
        return $this->belongsTo('App\User' );
    }
    public function dailystocks_daysheet()
    {
        return $this->belongsTo('App\Models\DaySheet' );
    }
    public function dailystocks_chemtypes()
    {
        return $this->hasMany('App\Models\Admin\ChemType' );
    }
}
