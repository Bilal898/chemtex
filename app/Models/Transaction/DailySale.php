<?php

namespace App\Models\Transaction;

use Illuminate\Database\Eloquent\Model;

class DailySale extends Model
{
  protected $fillable = [


  'chem_type1_sale','chem_type2_sale', 'chem_type3_sale',
  'chem_type4_sale', 'chem_type5_sale',


    ];

    public function dailysales_user()
    {
        return $this->belongsTo('App\User' );
    }
    public function dailysales_daysheet()
    {
        return $this->belongsTo('App\Models\DaySheet' );
    }
    public function dailysales_chemtypes()
    {
        return $this->hasMany('App\Models\Admin\ChemType' );
    }
}
