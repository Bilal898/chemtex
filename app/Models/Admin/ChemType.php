<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class ChemType extends Model
{
  protected $fillable = [
    'chem_name', 'is_stocked',
    'is_active', 'date_added', 'chem_type_no',
];

  public function chem_type_user()
    {
        return $this->belongsTo('App\User');
    }
  // public function chem_type_dailysale()
  //   {
  //       return $this->belongsTo('App\Models\Transaction\DailySale');
  //   }
  // public function chem_type_dailystock()
  //   {
  //       return $this->belongsTo('App\Models\Transaction\DailyStock');
  //   }
  public function chem_daysheet()
    {
        return $this->belongsTo('App\Models\DaySheet');
    }
}
