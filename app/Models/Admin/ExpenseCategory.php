<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class ExpenseCategory extends Model
{
  protected $fillable = [
    'expense_category_name',
  ];

    public function expense_transactions()
    {
        return $this->belongsTo('App\Models\Admin\ExpenseTransaction');

    }
    public function expcat_user()
    {
        return $this->belongsTo('App\User');

    }
}
