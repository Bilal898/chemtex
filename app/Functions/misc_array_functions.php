<?php



function create_req_array_unfilled($requestArray){
  $result = array();

  foreach($requestArray as $key=>$value){
    $result[] = $value;
    }
    return array_filter($result);
}

function create_req_array($requestArray){
  $result = array_fill(0, 10, null);
  $reqArrayTrim = array_filter($requestArray);

  foreach($reqArrayTrim as $key=>$value){
    $result[$key] = $value;
    }
    return $result;
}
