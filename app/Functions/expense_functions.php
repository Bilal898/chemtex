<?php

use App\Models\Admin\ExpenseCategory;
use App\User;

      function  create_expense_category_db($reqArrayExpCat){
        $user = User::find(Auth::id());
        $newArray = array();
        foreach(array_filter($reqArrayExpCat) as $key=>$value){
          $newuserExpType = ExpenseCategory::create([
               'expense_category_name' => $value,
                ]);
          $user->expense_category()->save($newuserExpType);
            }

      }


      function create_expense_category_array(){
        $userExpCatArray = DB::table('expense_categories')->where([
                  ['user_id', '=', Auth::id()],
                              ])->get();
        return $userExpCatArray;

      }


      function expense_category_count(){

        $userExpCatCount = DB::table('expense_categories')->where([
                  ['user_id', '=', Auth::id()],
                              ])->count();

          return $userExpCatCount;

      }
