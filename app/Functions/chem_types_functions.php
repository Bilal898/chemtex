<?php

use App\Models\Admin\ChemType;
use App\User;

      function  create_chem_type_db($reqArrayChemType){
        $user = User::find(Auth::id());
        $newArray = array();
        foreach(array_filter($reqArrayChemType) as $key=>$value){
          $newuserChemType = ChemType::create([
               'chem_name' => $value,
                ]);
          $user->chem_types()->save($newuserChemType);
            }

      }



      function create_chem_type_array(){
        $userChemTypesColl = ChemType::where([
                  ['user_id', '=', Auth::id()],
                  ['is_active', '=', '1'],
                              ])->orderBy('id', 'desc')->get();


        return   $userChemTypesColl;

      }

      function create_chem_type_name_array(){
        $userChemTypesColl = ChemType::where([
                  ['user_id', '=', Auth::id()],
                  ['is_active', '=', '1'],
                              ])->orderBy('id', 'desc')->get();
        $userChemTypeArr = $userChemTypesColl->toArray();
        $userChemTypeArray = array();

        for($i =0; $i < count($userChemTypeArr); $i++){
                 $temp = array_only($userChemTypeArr[$i],[
                  'chem_name',
                      ]);
                  $userChemTypeArray[] = $temp['chem_name'];
              }

        return  $userChemTypeArray;

      }
      function chem_type_count(){

        $chemTypeTblCount = DB::table('chem_types')->where([
                  ['user_id', '=', Auth::id()],
                  ['is_active', '=', '1'],
                              ])->count();

          return $chemTypeTblCount;

      }
