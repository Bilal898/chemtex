<?php

use App\Models\DaySheet;
use App\User;


    function create_daysheet_array(){

      $userDaySheetColl = DaySheet::where([
                ['user_id', '=', Auth::id()],
                            ])->orderBy('id', 'desc')->get();
      return $userDaySheetColl;

}
    function create_daysheet_db($ds_date){
          $user = User::find(Auth::id());
          $newDaySheet = DaySheet::create([
               'daysheet_date' => $ds_date,
                ]);
            $user->day_sheets()->save($newDaySheet);
            return  $newDaySheet;
    }
    function day_sheet_count(){

      $daySheetCount = DB::table('day_sheets')->where([
                ['user_id', '=', Auth::id()],
                            ])->count();

        return $daySheetCount;

    }
    function last_day_sheet_id(){

      $lastDaySheetID = DB::table('day_sheets')
                            ->where('user_id',  Auth::id())->max('id');

        return $lastDaySheetID;

    }
