<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function gravatar()
    {
        $email = $this->email;
        $default = "https://upload.wikimedia.org/wikipedia/commons/thumb/5/50/User_icon-cp.svg/200px-User_icon-cp.svg.png";
        $size = 100;

        return "https://www.gravatar.com/avatar/" . md5( strtolower( trim( $email ) ) ) . "?d=" . urlencode( $default ) . "&s=" . $size;
    }

    public function getBioHtmlAttribute($value)
    {
        return $this->bio ? Markdown::convertToHtml(e($this->bio)) : NULL;
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function chem_types()
      {
          return $this->hasMany('App\Models\Admin\ChemType');
      }
    public function daily_sales()
      {
          return $this->hasMany('App\Models\Transaction\DailySale');
      }
    public function daily_stocks()
      {
          return $this->hasMany('App\Models\Transaction\DailyStock');
      }
    public function day_sheets()
      {
          return $this->hasMany('App\Models\DaySheet');
      }
      public function expense_category()
      {
          return $this->hasMany('App\Models\Admin\ExpenseCategory');
      }
}
