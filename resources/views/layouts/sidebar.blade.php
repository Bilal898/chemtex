<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <!-- <div class="user-panel">
      <div class="pull-left image">
        <img src="{{ Auth::user()->gravatar() }}" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p>{{ Auth::user()->name }}</p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div> -->

    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
      <li>
        <a href="{{ url('/home') }}">
          <i class="fa fa-dashboard"></i> <span>Dashboard</span>
        </a>
      </li>
      <li><a href="{{ route('daysheets.index') }}"><i class="fa fa-pencil"></i> <span>All Daysheets</span></a></li>
      <li><a href="{{ route('daysheets.create') }}"><i class="fa fa-pencil"></i> <span>Add DaySheet</span></a></li>


      <li class="treeview">
        <a href="#">
          <i class="fa fa-folder"></i>
          <span>Accounts</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{ route('daily_profit.index') }}"><i class="fa fa-pencil"></i> <span>Profit Report</span></a></li>
          <li><a href="{{ route('daily_profit.index') }}"><i class="fa fa-pencil"></i> <span>Capital Report</span></a></li>
        </ul>
      </li>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-folder"></i>
          <span>Transactions</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{ route('purchase_invoices.index') }}"><i class="fa fa-circle-o"></i>Purchase Invoices</a></li>
        </ul>
      </li>

      <li class="treeview">
        <a href="#">
          <i class="fa fa-folder"></i>
          <span>Reports</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{ route('daysheets.index') }}"><i class="fa fa-circle-o"></i>Sales</a></li>
          <li><a href="{{ route('daysheets.create') }}"><i class="fa fa-circle-o"></i>Stock</a></li>
          <li><a href="{{ route('daysheets.index') }}"><i class="fa fa-circle-o"></i>Purchase Invoice</a></li>
          <li><a href="{{ route('daysheets.create') }}"><i class="fa fa-circle-o"></i>Expenses</a></li>
          <li><a href="{{ route('daysheets.index') }}"><i class="fa fa-circle-o"></i>Salaries</a></li>
        </ul>
      </li>

      <li class="treeview">
        <a href="#">
          <i class="fa fa-folder"></i>
          <span>Accounts</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{ route('daily_profit.index') }}"><i class="fa fa-circle-o"></i>Monthly Profit</a></li>
          <li><a href="{{ route('expense_transaction.index') }}"><i class="fa fa-circle-o"></i>Monthly Capital</a></li>
          <li><a href="{{ route('expense_transaction.index') }}"><i class="fa fa-circle-o"></i>Cost of Sale</a></li>
          <li><a href="{{ route('expense_transaction.index') }}"><i class="fa fa-circle-o"></i>Profit Margins</a></li>
        </ul>
      </li>

      <li class="treeview">
        <a href="#">
          <i class="fa fa-folder"></i>
          <span>Settings</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{ route('chem_types.index') }}"><i class="fa fa-circle-o"></i>Chemical Types</a></li>
          <li><a href="{{ route('expense_category.index') }}"><i class="fa fa-circle-o"></i>Expense Categories</a></li>
          <li><a href="{{ route('chem_sale_price.index') }}"><i class="fa fa-circle-o"></i>Chemical Sale Prices</a></li>
        </ul>
      </li>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-folder"></i>
          <span>Profile</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{ route('daysheets.index') }}"><i class="fa fa-circle-o"></i>User Info</a></li>
          <li><a href="{{ route('daysheets.index') }}"><i class="fa fa-circle-o"></i>Billing</a></li>
          <li><a href="{{ route('daysheets.index') }}"><i class="fa fa-circle-o"></i>Support</a></li>
        </ul>
      </li>
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>
