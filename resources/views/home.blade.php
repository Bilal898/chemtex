@extends('layouts.main')




@section('content')
<form>
{{ csrf_field() }}
</form>
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          SM Dasbhboard
        </h1>
        <ol class="breadcrumb">
          <li class="active"><i class="fa fa-dashboard"></i>Dashboard</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <!-- /.box-header -->
                <div class="box-body ">
                      <h3>Welcome to Site Manager!</h3>
                      <p class="lead text-muted">Hallo {{ Auth::user()->name }}, your last login was at:</p>

                      <h4>Get started</h4>
                      <p><a href="#" class="btn btn-primary">Create your first Day Sheet!</a> </p>
                </div>
                <!-- /.box-body -->
              </div>
              <!-- /.box -->
            </div>
          </div>
        <!-- ./row -->
      </section>
      <!-- /.content -->
    </div>

<!-- <input type="hidden" name="_token" value="{{ Session::token() }}"> -->
@endsection
