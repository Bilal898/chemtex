
    <script type="text/javascript">
    $(document).ready(function(){
        //group add limit
        var maxGroup = 10;
        var totalExpensesFld = maxGroup;
        var oneAlertMsg = 0;

        //add more Expenses group
        $(".addMoreExpenses").click(function(e){
            if($('body').find('.fieldGroupExpenses').length < totalExpensesFld){
                var fieldHTML = '<div class="fieldGroupExpenses">'+$(".fieldGroupExpensesCopy").html()+'</div>';
                $('body').find('.fieldGroupExpenses:last').after(fieldHTML);
            }else{
              if(oneAlertMsg == 0){
                    alertMsg = 'Maximum '+totalExpensesFld+' Expense Transactions are allowed.';
                    $('body').find('.jqry_Error').after(alertMsg);
                    ++oneAlertMsg;
                    }            }
        });

        //remove fields group
        $("body").on("click",".remove",function(){
            $(this).parents(".fieldGroupExpenses").remove();
        });

    });
    </script>
