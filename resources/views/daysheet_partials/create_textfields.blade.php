
<!-- /////////////////////////////// DAILY SALE TEXTFIELDS -->
<div class="table-responsive">
  <table class="table">
      <thead class="thead">

        <tr>
          <th scope="col"></th>


        <!-- /////////////////////////////// HEADERS -->
        @php  $text = "<th scope=\"col\">";
              $text2 = "</th>";

        @endphp

          @for ($j = 1; $j <= $userChemTypeCount; $j++)
            {!! $text !!}{{ $j }}{{"-"}}{{ $userChemTypeNameArray[$j-1] }}{!! $text2 !!}
          @endfor

          </tr>
          </thead>
          <tbody>


      <tr>
        <td><input type="date" class="form-control" name="ds_date"></td>
        @php $text1 = "<td>";
              $text2 = "<input type=\"text\" class=\"form-control\" id=\"chem_type_sale";
              $text3 = "\" name=\"chem_type_sale";
              $text4 = "\">";
              $text5 = "</td>";
              @endphp
         <!-- unit fields -->
         @for ($j = 1; $j <= $userChemTypeCount; $j++)
         {!! $text1 !!}
         {!! $text2 !!}{{ $j }}{!! $text3 !!}{{ "[]" }}{!! $text4 !!}
         {!! $text5 !!}
         @endfor
    </tr>

    </tbody>
  </table>
</div>

<!-- /////////////////////////////// DAILY STOCK -->
<div class="table-responsive">
  <table class="table">
      <thead class="thead">

        <tr>
          <th scope="col"></th>



        @php  $text = "<th scope=\"col\">";
              $text2 = "</th>";

        @endphp

          @for ($j = 1; $j <= $userChemTypeStockCount; $j++)
            {!! $text !!}{{ $j }}{{"-"}}{{ $userChemTypeStockArray[$j-1] }}{!! $text2 !!}
          @endfor

          </tr>
          </thead>
          <tbody>


      <tr>
        <td></td>
        @php $text1 = "<td>";
              $text2 = "<input type=\"text\" class=\"form-control\" id=\"chem_type_stock";
              $text3 = "\" name=\"chem_type_stock";
              $text4 = "\">";
              $text5 = "</td>";
              @endphp

         @for ($j = 1; $j <= $userChemTypeStockCount; $j++)
         {!! $text1 !!}
         {!! $text2 !!}{{ $j }}{!! $text3 !!}{{ "[]" }}{!! $text4 !!}
         {!! $text5 !!}
         @endfor
    </tr>

    </tbody>
  </table>
</div>
