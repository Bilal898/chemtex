<div class="fieldGroupCopy"  style="display: none;">
      <div class="row" style="padding:5px;">
        <div class="col-lg-6">
          <input type="text" class="form-control" name="expense_title[]">
        </div>
        <div class="col-lg-2">
          <input type="text" class="form-control" name="amount[]">
        </div>
        <div class="col-lg-2">
          <select class="form-control" id="category" name="category[]">
              <option value="0" selected></option>
              @foreach($categories as $category)
                  <option value="{{ $category->id }}">{{ $category->name }}</option>
              @endforeach
            </select>
        </div>
        <div class="col-lg-2">
          <a href="javascript:void(0)" class="btn btn-danger remove">
            <span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true">
            </span> Remove
          </a>
        </div>
      </div>
    </div>
<div class="fieldGroupSettlementsCopy"  style="display: none;">
      <div class="row" style="padding:5px;">
        <div class="col-lg-2">
          <label for="amount">Amount</label>
          <input type="text" class="form-control" name="card_amount[]">
        </div>
        <div class="col-lg-3">
          <label for="category">Card Type</label>
          <select class="form-control" id="exp_category" name="exp_category[]">
              <option value="" selected></option>
              @foreach($categories as $key=>$value)
                  <option value="{{ $value }}">{{ $value }}</option>
              @endforeach
            </select>
        </div>
        <div class="col-lg-2">
          <a href="javascript:void(0)" class="btn btn-danger remove">
            <span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true">
            </span> Remove
          </a>
        </div>
      </div>
    </div>

    <script type="text/javascript">
    $(document).ready(function(){
        //group add limit
        var maxGroup = 10;

        //add more fields group
        $(".addMore").click(function(e){
            if($('body').find('.fieldGroup').length < maxGroup){
                var fieldHTML = '<div class="fieldGroup">'+$(".fieldGroupCopy").html()+'</div>';
                $('body').find('.fieldGroup:last').after(fieldHTML);
            }else{
                alert('Maximum '+maxGroup+' groups are allowed.');
            }
        });
        //add more fields group
        $(".addMore1").click(function(e){
            if($('body').find('.fieldGroup1').length < maxGroup){
                var fieldHTML = '<div class="fieldGroup1">'+$(".fieldGroupCopy1").html()+'</div>';
                $('body').find('.fieldGroup1:last').after(fieldHTML);
            }else{
                alert('Maximum '+maxGroup+' groups are allowed.');
            }
        });

        //remove fields group
        $("body").on("click",".remove",function(){
            $(this).parents(".fieldGroup").remove();
        });

        $("body").on("click",".remove1",function(){
            $(this).parents(".fieldGroup1").remove();
        });

    });
    </script>
