<div class="fieldGroupExpensesCopy"  style="display: none;">
      <div class="row" style="padding:5px;">
        <div class="col-lg-3">
          <select class="form-control" id="exp_category" name="exp_category[]">
              <option value="" selected></option>
              @foreach($categories as $key=>$value)
                  <option value="{{ $value }}">{{ $value }}</option>
              @endforeach
            </select>
        </div>
        <div class="col-lg-2">
          <input type="text" class="form-control" name="expense_amount[]">
        </div>
        <div class="col-lg-5">
          <input type="text" class="form-control" name="expense_desc[]">
        </div>


        <div class="col-lg-2">
          <a href="javascript:void(0)" class="btn btn-danger remove">
            <span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true">
            </span> Remove
          </a>
        </div>
      </div>
    </div>


    
