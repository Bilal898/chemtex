<?php
      if (($request->input('exp_category')[0]!=null) &&  ($request->input('expense_amount')[0]!=null))
      {
// dd("hello");
        $reqArrayExpDesc = array_fill(0, 10, null);
        $reqArrayTrim = array_filter($request->expense_desc);
        $reqArrayCount = count($reqArrayTrim);

        if($reqArrayCount > 10){
          return redirect('daysheet/index')->with('status', 'Maximum number of Expense Transactions is 10!');
          }
          //expense_desc array
        foreach($reqArrayTrim as $key=>$value){
          $reqArrayExpDesc[$key] = $value;
          }
        //expense_amount array

        $reqArrayExpAmount = array_fill(0, 10, null);
        $reqArrayTrim = array_filter($request->expense_amount);
        $reqArrayCount = count($reqArrayTrim);

        foreach($reqArrayTrim as $key=>$value){
          $reqArrayExpAmount[$key] = $value;
          }
        //expense_ Category array
        $reqArrayExpCat = array_fill(0, 10, null);
        $reqArrayTrim = array_filter($request->exp_category);
        $reqArrayCount = count($reqArrayTrim);

        foreach($reqArrayTrim as $key=>$value){
          $reqArrayExpCat[$key] = $value;
          }
    // dd($users);
        $newExpenseTransaction =  ExpenseTransaction::create([
            'expense_desc1' => $reqArrayExpDesc[0],
            'expense_amount1' => $reqArrayExpAmount[0],
            'expense_category_id1' => $reqArrayExpCat[0],
            'expense_desc2' => $reqArrayExpDesc[1],
            'expense_amount2' => $reqArrayExpAmount[1],
            'expense_category_id2' => $reqArrayExpCat[1],
            'expense_desc3' => $reqArrayExpDesc[2],
            'expense_amount3' => $reqArrayExpAmount[2],
            'expense_category_id3' => $reqArrayExpCat[2],
          ]);

          foreach($reqArrayExpCat as $key=>$value){
            $expCatForeignKey = DB::table('expense_categories')
                    ->where('expense_category_name', $value)->first();
            if(!empty($expCatForeignKey) && $expCatForeignKey->expense_transaction_id == null){
              $expenseTransactionCategory =  ExpenseCategory::find($expCatForeignKey->id);
              $newExpenseTransaction->expense_category()->save($expenseTransactionCategory);
            }

            }


          $newdaysheet->expense_transactions()->save($newExpenseTransaction);

}  //end ELSE 
