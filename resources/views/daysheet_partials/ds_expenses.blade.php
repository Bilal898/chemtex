@empty($userExpCatCount)
<tr>
     <th colspan="12" class="text-center">No Expense Categories Saved.
       <a href="{{ route('expense_category.index') }}" class="btn btn-xs btn-info">
         Create New Expense Category
       </a>
     </th>
</tr>
@endempty
@if (!empty($userExpCatCount))
<div class="panel panel-info">
          <div class="panel-heading"><h4>Expenses</h4></div>
          <div class="panel-body">
            <table class="table">

<!-- <div class="container"> -->
    <div class="fieldGroupExpenses">
        <div class="row" style="padding:5px;">
          <div class="col-lg-3">
            <label for="category">Category</label>
            <select class="form-control" id="exp_category" name="exp_category[]">
                <option value="" selected></option>
                @foreach($userExpCatArray as $key=>$value)
                    <option value="{{ $value }}">{{ $value }}</option>
                @endforeach
              </select>
          </div>
          <div class="col-lg-2">
            <label for="amount">Amount</label>
            <input type="text" class="form-control" name="expense_amount[]">
          </div>
          <div class="col-lg-5">
            <label for="expense_title">Expense Detail</label>
            <input type="text" class="form-control" name="expense_desc[]">
          </div>


          <div class="col-lg-2" style="margin-top:25px;">
            <a href="javascript:void(0)" class="btn btn-success addMoreExpenses">
              <span class="glyphicon glyphicon glyphicon-plus" aria-hidden="true">
              </span> Add
            </a>
          </div>
        </div>
      </div>
<!-- </div> -->
</table>
</div>
</div>
@endif
