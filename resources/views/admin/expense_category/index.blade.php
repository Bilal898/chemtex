@extends('layouts.main')

@section('content')

<div class="content-wrapper">

  <section class="content-header">
    <h4>Create Expense Categories</h4>
    @include('partials.errors')
          <div class="maxExpCatError" id="maxExpCatError">
          </div>
    <ol class="breadcrumb">
      <li class="active"><i class="fa fa-dashboard"></i>Settings</li>
    </ol>
  </section>
<div class="panel panel-info">
  <!-- <div class="panel-heading"><h4>Create Chem Type</h4></div> -->

<div class="panel-body">

  <div class="panel panel-info">
            <div class="panel-heading"><h4>Expense Category Name</h4></div>
      <div class="panel-body">
                <form action="{{ route('expense_category.store') }}" method="post">
                       {{ csrf_field() }}
<table>
                       <div class="fieldGroup">
                         <div class="row">
                       <div class="form-group col-lg-4">
                             <label for="name">Name</label>
                             <input type="text" name="exp_cat_name[]" class="form-control">
                       </div>
                       <div class="form-group col-lg-4">
                             <label for="name">Name</label>
                             <input type="text" name="exp_cat_name[]" class="form-control">
                       </div>
                       <div class="col-lg-2" style="margin-top:25px;">
                       <a href="javascript:void(0)" class="btn btn-danger addMore">
                         <span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true">
                         </span> Add
                       </a>
                       </div>
                       </div>
                       </div>
</table>


                       <!-- <div class="row"> -->
                       <div class="form-group col-lg-8" style="margin-top:25px;">
                       <!-- <div class="form-group col-lg-8"> -->
                             <div class="text-center">
                                   <button class="btn btn-success" type="submit">
                                         Create
                                   </button>
                             </div>
                       </div>
                       <!-- </div> -->
                 </form>
           </div>
           </div>

           <div class="panel panel-info">
                     <div class="panel-heading"><h4>Expense Categories List</h4></div>
               <div class="panel-body">


                     <table class="table table-hover">
           @if($userExpCatCount > 0)
                           <thead>
                                 <th>
                                  ID
                                 </th>
                           </thead>
                           <tbody>
               @foreach($userExpCatArray as $cat)
                           <tr>
                       <td>{{ $loop->index }}{{ "- ".$cat->expense_category_name }}</td>
                 <td>
                         <form action="{{ route('expense_category.destroy' , ['id' => $cat->id])  }}" method="POST">

                             <input name="_method" type="hidden" value="DELETE">
                             {{ csrf_field() }}
                                 <button type="submit" class="btn btn-xs btn-danger">Delete</button>
                       </form>
                 </td>
                                                   </tr>
                   @endforeach
                           </tbody>
             @endif
   @empty ($userExpCatCount)
   <tr>
        <th colspan="9" class="text-center">No Expense Categories Saved
          <div class="btn btn-xs btn-info">
            Create New Expense Categories
          </div>
        </th>
   </tr>
   @endempty
                     </table>

               </div>

            </div>
      </div>
      </div>
      </div>

      <div class="fieldGroupCopy"  style="display: none;">
        <div class="row">
      <div class="form-group col-lg-4">
            <!-- <label for="name">Name</label> -->
            <input type="text" name="exp_cat_name[]" class="form-control">
      </div>
      <div class="form-group col-lg-4">
            <!-- <label for="name">Name</label> -->
            <input type="text" name="exp_cat_name[]" class="form-control">
      </div>
      <div class="col-lg-2">
      <a href="javascript:void(0)" class="btn btn-danger remove">
        <span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true">
        </span> Remove
      </a>
      </div>
      </div>
      </div>

      <script type="text/javascript">
      $(document).ready(function(){
          //group add limit
          var maxGroup = 5;
          var totalChemTypes = maxGroup * 2;
          var oneAlertMsg = 0;

          //add more fields group
          $(".addMore").click(function(e){
              if($('body').find('.fieldGroup').length < maxGroup){
                  var fieldHTML = '<div class="fieldGroup">'+$(".fieldGroupCopy").html()+'</div>';
                  $('body').find('.fieldGroup:last').after(fieldHTML);
              }else{
                  if(oneAlertMsg == 0){
                        alertMsg = 'Maximum '+totalChemTypes+' expense categories are allowed.';
                        $('body').find('.maxExpCatError').after(alertMsg);
                        ++oneAlertMsg;
                        }
              }
          });
          //add more fields group
          $(".addMore1").click(function(e){
              if($('body').find('.fieldGroup1').length < maxGroup){
                  var fieldHTML = '<div class="fieldGroup1">'+$(".fieldGroupCopy1").html()+'</div>';
                  $('body').find('.fieldGroup1:last').after(fieldHTML);
              }else{
                  alert('Maximum '+maxGroup+' expense categories are allowed.');
              }
          });

          //remove fields group
          $("body").on("click",".remove",function(){
              $(this).parents(".fieldGroup").remove();
          });

          $("body").on("click",".remove1",function(){
              $(this).parents(".fieldGroup1").remove();
          });

      });
      </script>
@endsection
