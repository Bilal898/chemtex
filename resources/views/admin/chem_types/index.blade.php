@extends('layouts.main')

@section('content')

<div class="content-wrapper">

  <section class="content-header">
    <h4>Create Chemical Type</h4>
    @include('partials.errors')
          <div class="maxChemicalTypesError" id="maxChemicalTypesError">
          </div>
    <ol class="breadcrumb">
      <li class="active"><i class="fa fa-dashboard"></i>Settings</li>
    </ol>
  </section>
<div class="panel panel-info">

<div class="panel-body">

  <div class="panel panel-info">
            <div class="panel-heading"><h4>Chemical Type Name</h4></div>
      <div class="panel-body">
                <form action="{{ route('chem_types.store') }}" method="post">
                       {{ csrf_field() }}
<table>
                       <div class="fieldGroup">
                         <div class="row">
                       <div class="form-group col-lg-4">
                             <label for="name">Name</label>
                             <input type="text" name="chem_type_name[]" class="form-control">
                       </div>
                       <div class="form-group col-lg-4">
                             <label for="name">Name</label>
                             <input type="text" name="chem_type_name[]" class="form-control">
                       </div>
                       <div class="col-lg-2" style="margin-top:25px;">
                       <a href="javascript:void(0)" class="btn btn-danger addMore">
                         <span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true">
                         </span> Add
                       </a>
                       </div>
                       </div>
                       </div>
</table>


                       <!-- <div class="row"> -->
                       <div class="form-group col-lg-8" style="margin-top:25px;">
                       <!-- <div class="form-group col-lg-8"> -->
                             <div class="text-center">
                                   <button class="btn btn-success" type="submit">
                                         Save
                                   </button>
                             </div>
                       </div>
                       <!-- </div> -->
                 </form>
           </div>
           </div>
<!-- ///////////////////LIST -->
           <div class="panel panel-info">
                     <div class="panel-heading"><h4>Chemical Type List</h4></div>
               <div class="panel-body">



                  <table class="table table-hover">
        @if($userChemTypesCount > 0)
                        <thead>
                              <th>
                                DaySheet
                              </th>
                                    <th>{{ "Chemical " }}</th>
                        </thead>
                        <tbody>
                          @foreach($userChemTypeArray as $chem)
                                      <tr>

                                        <td>{{ $chem->chem_type_user['id'] }}  </td>


                                        <td>{{ $chem->chem_name }}  </td>
                            <td>
                                  <form action="{{ route('chem_types.destroy' , ['id' => $chem->id])  }}" method="POST">
                                        <input name="_method" type="hidden" value="DELETE">
                                        {{ csrf_field() }}
                                            <button type="submit" class="btn btn-xs btn-danger">Delete</button>
                                  </form>
                            </td>
                                                              </tr>
                              @endforeach
                        </tbody>
          @endif
@empty ($userChemTypesCount)
<tr>
     <th colspan="9" class="text-center">No Chemical Type Saved
       <div class="btn btn-xs btn-info">
         Create New Chemical Type
       </div>
     </th>
</tr>
@endempty
                  </table>

            </div>
      </div>
      </div>
      </div>

      <div class="fieldGroupCopy"  style="display: none;">
        <div class="row">
      <div class="form-group col-lg-4">
            <input type="text" name="chem_type_name[]" class="form-control">
      </div>
      <div class="form-group col-lg-4">
            <input type="text" name="chem_type_name[]" class="form-control">
      </div>
      <div class="col-lg-2">
      <a href="javascript:void(0)" class="btn btn-danger remove">
        <span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true">
        </span> Remove
      </a>
      </div>
      </div>
      </div>

      <script type="text/javascript">
      $(document).ready(function(){
          var maxGroup = 5;
          var totalChemicalTypes = maxGroup * 2;
          var oneAlertMsg = 0;

          $(".addMore").click(function(e){
              if($('body').find('.fieldGroup').length < maxGroup){
                  var fieldHTML = '<div class="fieldGroup">'+$(".fieldGroupCopy").html()+'</div>';
                  $('body').find('.fieldGroup:last').after(fieldHTML);
              }else{
                  if(oneAlertMsg == 0){
                        alertMsg = 'Maximum '+totalChemicalTypes+' Chemical Types are allowed.';
                        $('body').find('.maxChemicalTypesError').after(alertMsg);
                        ++oneAlertMsg;
                        }
              }
          });


          //remove fields group
          $("body").on("click",".remove",function(){
              $(this).parents(".fieldGroup").remove();
          });


      });
      </script>
@endsection
