@extends('layouts.main')

@section('content')

<div class="content-wrapper">

  <section class="content-header">
    <h4>Create Chem Purchases</h4>
    @include('partials.errors')
          <div class="maxExpCatError" id="maxExpCatError">
          </div>
    <ol class="breadcrumb">
      <li class="active"><i class="fa fa-dashboard"></i>Invoices</li>
    </ol>
  </section>
<div class="panel panel-info">
  <!-- <div class="panel-heading"><h4>Create Chem Type</h4></div> -->

  <div class="panel-body">

  <div class="panel panel-info">
            <div class="panel-heading"><h4>Invoice Details</h4></div>
      <div class="panel-body">
                <form action="{{ route('purchase_invoices.store') }}" method="post">
                       {{ csrf_field() }}
     <table>


        <!-- //////// fields-->
                       <div class="fieldGroup">
                         <div class="row">
                       <div class="form-group col-lg-2">
                             <label for="name">Chem Type</label>
                             <input type="text" name="inv_date[]" class="form-control">
                       </div>
                       <div class="form-group col-lg-2">
                             <label for="name">Date</label>
                             <input type="text" name="inv_date[]" class="form-control">
                       </div>
                       <div class="form-group col-lg-2">
                             <label for="name">Invoice No.</label>
                             <input type="text" name="inv_no[]" class="form-control">
                       </div>
                       <div class="form-group col-lg-2">
                             <label for="name">SAP No.</label>
                             <input type="text" name="inv_sap_no[]" class="form-control">
                       </div>
                       <div class="form-group col-lg-1">
                             <label for="name">Quantity</label>
                             <input type="text" name="inv_qty[]" class="form-control">
                       </div>
                       <div class="form-group col-lg-1">
                             <label for="name">Amount</label>
                             <input type="text" name="inv_amt[]" class="form-control">
                       </div>
                       <div class="col-lg-2" style="margin-top:25px;">
                       <a href="javascript:void(0)" class="btn btn-danger addMore">
                         <span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true">
                         </span> Add
                       </a>
                       </div>
                       </div>
                       </div>
</table>


                       <!-- <div class="row"> -->
                       <div class="form-group col-lg-8" style="margin-top:25px;">
                       <!-- <div class="form-group col-lg-8"> -->
                             <div class="text-center">
                                   <button class="btn btn-success" type="submit">
                                         Create
                                   </button>
                             </div>
                       </div>
                       <!-- </div> -->
                 </form>
           </div>
           </div>

           <div class="panel panel-info">
                     <div class="panel-heading"><h4>Purchase Purchases List</h4></div>
               <div class="panel-body">


                 <!-- Main content -->
                 <section class="content">
                     <div class="row">
                       <div class="col-xs-12">
                         <div class="box">
                           <!-- /.box-header -->
                           <div class="box-body ">
                             <div class="table-responsive">
                               <table class="table table-striped">
                                 <thead>
                                   <tr>
                                     <th scope="col"></th>
                                     <th scope="col">Date</th>

                                     <th scope="col">Invoice No.</th>
                                     <th scope="col">SAP No. </th>
                                     <th scope="col">Quantity</th>
                                     <th scope="col">Amount</th>
                                     <th scope="col">Margin</th>
                                     <!-- <th scope="col">Cheques</th>
                                     <th scope="col">Bank Deposit</th> -->
                                   </tr>
                                 </thead>
                                 <tbody>
                 <!-- check if categories exist -->

                             @empty($purchaseInvoiceCount)
                             <tr>
                                  <th colspan="12" class="text-center">No Invoice Saved.
                                    <a href="" class="btn btn-xs btn-info">
                                      Create New Invoices
                                    </a>
                                  </th>
                            </tr>
                            @endempty



                                 </tbody>
                               </table>
                               </div>
                               </div>
                               <!-- /.box-body -->
                             </div>
                             <!-- /.box -->
                           </div>
                             <!-- /.col-xs-12 -->
                         </div>
                       <!-- ./row -->
                     </section>

               </div>

            </div>
      </div>
      </div>
      </div>

      <div class="fieldGroupCopy"  style="display: none;">
        <div class="row">
          <div class="form-group col-lg-2">
                <input type="text" name="inv_date[]" class="form-control">
          </div>
          <div class="form-group col-lg-2">
                <input type="text" name="inv_no[]" class="form-control">
          </div>
          <div class="form-group col-lg-2">
                <input type="text" name="inv_sap_no[]" class="form-control">
          </div>
          <div class="form-group col-lg-2">
                <input type="text" name="inv_qty[]" class="form-control">
          </div>
          <div class="form-group col-lg-2">
                <input type="text" name="inv_amt[]" class="form-control">
          </div>
      <div class="col-lg-2">
      <a href="javascript:void(0)" class="btn btn-danger remove">
        <span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true">
        </span> Remove
      </a>
      </div>
      </div>
      </div>

      <script type="text/javascript">
      $(document).ready(function(){
          //group add limit
          var maxGroup = 5;
          var totalChemTypes = maxGroup * 2;
          var oneAlertMsg = 0;

          //add more fields group
          $(".addMore").click(function(e){
              if($('body').find('.fieldGroup').length < maxGroup){
                  var fieldHTML = '<div class="fieldGroup">'+$(".fieldGroupCopy").html()+'</div>';
                  $('body').find('.fieldGroup:last').after(fieldHTML);
              }else{
                  if(oneAlertMsg == 0){
                        alertMsg = 'Maximum '+totalChemTypes+' expense categories are allowed.';
                        $('body').find('.maxExpCatError').after(alertMsg);
                        ++oneAlertMsg;
                        }
              }
          });
          //add more fields group
          $(".addMore1").click(function(e){
              if($('body').find('.fieldGroup1').length < maxGroup){
                  var fieldHTML = '<div class="fieldGroup1">'+$(".fieldGroupCopy1").html()+'</div>';
                  $('body').find('.fieldGroup1:last').after(fieldHTML);
              }else{
                  alert('Maximum '+maxGroup+' expense categories are allowed.');
              }
          });

          //remove fields group
          $("body").on("click",".remove",function(){
              $(this).parents(".fieldGroup").remove();
          });

          $("body").on("click",".remove1",function(){
              $(this).parents(".fieldGroup1").remove();
          });

      });
      </script>
@endsection
