


<!-- <div class="d-flex flex-row fieldGroupCopy"  style="display: none;"> -->
<!-- <div class="d-flex flex-row fieldGroupCopy .d-none" style="display: none;"> -->
<!-- <div class="d-none d-flex flex-row fieldGroupCopy" style="display: none;"> -->
<!-- <div class="d-none d-flex flex-row fieldGroupCopy" style="d-none">

  <div class="p-2">
    <input type="text" class="form-control" placeholder="First name">
  </div>
  <div class="p-2">
    <input type="text" class="form-control" placeholder="Last name">
  </div>
  <div class="p-2">
  <label for="category" ></label>
      <select class="p-2" id="category" name="category[]">
          <option value="0" selected></option>
          @foreach($categories as $category)
              <option value="{{ $category->id }}">{{ $category->name }}</option>
          @endforeach
        </select>
    </div>
    <div class="p-2">
        <a href="javascript:void(0)" class="btn btn-secondary  btn-sm remove">
          <span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true">
          </span> Remove
        </a>
    </div>
  </div>
</div> -->


    <!-- copy of input fields group  related to js add remove -->
    <!-- <div class="card-body fieldGroupCopy" style="display: none;"> -->
    <!-- <div class="form-group fieldGroupCopy" style="display: none;"> -->
        <!-- <div class="form-group"> -->
            <!-- <div class="col-md-3 mb-3">
                <input type="text" name="name[]" class="form-control" placeholder="Enter name"/>
              </div>
              <div class="col-md-3 mb-3">
                <input type="text" name="email[]" class="form-control" placeholder="Enter email"/>
                </div> -->
                <!-- <div class="col-md-5"> -->
                  <!-- <label for="expense_title">Expense Title</label> -->
                  <!-- <label for="expense_title"></label>
                  <input type="text" name="expense_title[]" class="form-control"/>
                </div>
                <div class="col-md-2"> -->
                  <!-- <label for="amount">Amount</label> -->
                  <!-- <label for="amount"></label>
                  <input type="text" name="amount[]" class="form-control"/>
                </div>
                <div class="col-md-3"> -->
                <!-- <div class="col-4"> -->
                <!-- <label for="category" >Category</label> -->
                <!-- <label for="category" ></label>
                    <select class="form-control" id="category" name="category[]">
                        <option value="0" selected></option>
                        @foreach($categories as $category)
                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                        @endforeach
                      </select>
                  </div>
              <div class="col-md-2">
            <div class="btn btn-success-sm" style="margin-top:20px;"> -->
            <!-- <div class="input-group-addon"> -->
                <!-- <a href="javascript:void(0)" class="btn btn-danger remove">
                  <span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true">
                  </span> Remove
                </a>
            </div>
            </div> -->
            <!-- <div class="input-group-addon">
                <a href="javascript:void(0)" class="btn btn-danger remove"><span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span> Remove</a>
            </div> -->
        <!-- </div> -->
    <!-- </div> -->

    <div class="fieldGroupCopy"  style="display: none;">
    <!-- <div class="fieldGroupCopy invisible"> -->

      <div class="row m-2" style="margin-bottom: 5px;">
        <div class="col-lg-6">
          <input type="text" class="form-control" name="expense_title[]">
        </div>
        <div class="col-lg-2">
          <input type="text" class="form-control" name="amount[]">
        </div>
        <div class="col-lg-2">
          <select class="form-control" id="category" name="category[]">
              <option value="0" selected></option>
              @foreach($categories as $category)
                  <option value="{{ $category->id }}">{{ $category->name }}</option>
              @endforeach
            </select>
        </div>
        <div class="col-lg-1">
          <a href="javascript:void(0)" class="btn btn-danger remove">
            <span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true">
            </span> Remove
          </a>
        </div>
      </div>
    </div>

    <script type="text/javascript">
    $(document).ready(function(){
        //group add limit
        var maxGroup = 10;

        //add more fields group
        $(".addMore").click(function(){
            if($('body').find('.fieldGroup').length < maxGroup){
                // var fieldHTML = '<div class="d-flex flex-row fieldGroup">'+$(".fieldGroupCopy").html()+'</div>';
                // var fieldHTML = '<div class="d-flex flex-row fieldGroup">'+$(".fieldGroupCopy").html()+'</div>';
                var fieldHTML = '<div class="fieldGroup">'+$(".fieldGroupCopy").html()+'</div>';
                $('body').find('.fieldGroup:last').after(fieldHTML);
            }else{
                alert('Maximum '+maxGroup+' groups are allowed.');
            }
        });

        //remove fields group
        $("body").on("click",".remove",function(){
            $(this).parents(".fieldGroup").remove();
        });
        //add more fields group
        // $(".addMore").click(function(){
        //     if($('body').find('.fieldGroup').length < maxGroup){
        //         var fieldHTML = '<div class="form-group fieldGroup">'+$(".fieldGroupCopy").html()+'</div>';
        //         $('body').find('.fieldGroup:last').after(fieldHTML);
        //     }else{
        //         alert('Maximum '+maxGroup+' groups are allowed.');
        //     }
        // });
        //
        // //remove fields group
        // $("body").on("click",".remove",function(){
        //     $(this).parents(".fieldGroup").remove();
        // });
    });
    </script>
