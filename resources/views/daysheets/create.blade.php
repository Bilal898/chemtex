@extends('layouts.main')

@section('content')

<div class="content-wrapper">
    <section class="content-header">
      <h4>
      Create New Day Sheet
    </h4>
      @include('partials.errors')
            <div class="jqry_Error" id="jqry_Error">
            </div>
      <ol class="breadcrumb">
        <li class="active"><i class="fa fa-dashboard"></i>DaySheet</li>
      </ol>
    </section>

    <div class="panel panel-info">
      <div class="panel-body">

      <!-- Table -->
      <form method="post" action="{{ route('daysheets.store') }}">
    {{ csrf_field() }}

        <div class="panel panel-info">
              <div class="panel-heading"><h4>Sales and Stock</h4></div>
              <div class="panel-body">
                @include('daysheet_partials.create_textfields')
              </div>
        </div>

        <!-- /////////////////EXPENSES/////////////////////// -->
      @include('daysheet_partials.ds_expenses')


</div>
          <button type="submit" class="btn btn-primary">Save New Day Sheet</button>
</form>
</div>
<!-- </wrapper> -->
</div>
</div>
<!-- /////////////////////////////////////////////////////////////// -->

@include('daysheet_partials.fieldGroupCopy')
@include('daysheet_partials.js_daysheet')
@endsection
