@extends('layouts.main')

@section('content')



<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
    DaySheet List
  </h1>&nbsp;
    @include('partials.errors')
    <ol class="breadcrumb">
      <li class="active"><i class="fa fa-dashboard"></i>Dashboard</li>
    </ol>
  </section>

<!-- Main content -->
<section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <!-- /.box-header -->
          <div class="box-body ">
            <div class="table-responsive">
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th scope="col"></th>
                    <th scope="col">Date</th>
                    @for($e=0; $e < $chemTypeCount; $e++)
                    <th scope="col">{{ $userChemNameArray[$e] }}</th>
                    @endfor
                    <th scope="col">Expenses</th>
                    <th scope="col">HR</th>


                  </tr>
                </thead>
                <tbody>
<!-- check if categories exist -->
            @empty($chemTypeCount)
            <tr>


                 <th colspan="12" class="text-center">No Chemical Types Saved.
                   <a href="{{ route('chem_types.index') }}" class="btn btn-xs btn-info">
                     Create New Chemical Types
                   </a>
                 </th>
            </tr>
            @endempty




   <!-- check if day sheets exist -->
                      @if(!empty($daysheetCount))

                            @php
                              $rowcount = 1;
                            @endphp
                          @foreach($day_sheets as $day_sheet)
                      <tr>
                        <th scope="row">
                            @php
                              echo $rowcount++
                            @endphp
                        </th>
                        <td>{{ $day_sheet->daysheet_date }}</td>
                        <td>{{ $day_sheet->id }}</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>{{ $day_sheet->expenses_total }}</td>

                        <td>

                        @if ($day_sheet->id == $lastDaySheetID)
                          <a href="{{ route('daysheets.edit', ['id' => $day_sheet->id ]) }}" class="btn btn-xs btn-info">
                                Edit
                          </a>
                          <form action="{{ route('daysheets.destroy' , ['id' => $day_sheet->id ])  }}" method="POST">
                                <input name="_method" type="hidden" value="DELETE">
                                {{ csrf_field() }}
                                    <button type="submit" class="btn btn-xs btn-danger">Delete</button>
                          </form>
                          <a href="{{ route('daysheets.show', ['id' => $day_sheet->id ]) }}" class="btn btn-xs btn-primary">
                                View
                          </a>
                           @else
                           <a href="{{ route('daysheets.show', ['id' => $day_sheet->id ]) }}" class="btn btn-xs btn-primary">
                                 View
                           </a>
                          @endif
                        </td>
                      </tr>
                          @endforeach
                      @endif

                </tbody>
              </table>
              </div>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
          </div>
            <!-- /.col-xs-12 -->
        </div>
      <!-- ./row -->
    </section>
    <!-- /.content -->


            <!-- </div> -->
            </div>
            <!-- </div> -->
        <!-- </div>
        </div> -->
@endsection
