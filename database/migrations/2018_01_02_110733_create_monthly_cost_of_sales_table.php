<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMonthlyCostOfSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('monthly_cost_of_sales', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->nullable();
            $table->unsignedInteger('chem_type_id')->nullable();
            $table->unsignedInteger('monthly_profit_id')->nullable();
            $table->date('month_year')->nullable();

            $table->unsignedInteger('opening_stock_cost')->nullable();
            $table->unsignedInteger('monthly_avg_price_unit')->nullable();
            $table->unsignedInteger('monthly_total_quantity_sold')->nullable();
            $table->unsignedInteger('monthly_invoices_total_price')->nullable();
            $table->unsignedInteger('monthly_total_cost_of_sale')->nullable();
            $table->timestamps();

            $table->foreign('user_id')
                  ->references('id')->on('users')
                  ->onDelete('cascade');
            $table->foreign('chem_type_id')
                  ->references('id')->on('chem_types');
            $table->foreign('monthly_profit_id')
                  ->references('id')->on('monthly_profits');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('monthly_cost_of_sales');
    }
}
