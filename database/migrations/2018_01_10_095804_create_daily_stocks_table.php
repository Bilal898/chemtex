<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDailyStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('daily_stocks', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->nullable();
            $table->unsignedInteger('day_sheet_id')->nullable();
            $table->unsignedInteger('chem_type_id')->nullable();
            $table->unsignedInteger('daily_stock_quantity')->nullable();
            $table->unsignedInteger('opening_avg_price_unit')->nullable();
            $table->timestamps();

            $table->foreign('user_id')
                  ->references('id')->on('users')
                  ->onDelete('cascade');
            $table->foreign('day_sheet_id')
                  ->references('id')->on('day_sheets')
                  ->onDelete('cascade');
            $table->foreign('chem_type_id')
                  ->references('id')->on('chem_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('daily_stocks');
    }
}
