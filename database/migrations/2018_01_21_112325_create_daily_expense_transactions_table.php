<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDailyExpenseTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('daily_expense_transactions', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('user_id')->nullable();
            $table->unsignedInteger('day_sheet_id')->nullable();
            $table->unsignedInteger('expense_category_id')->nullable();
            $table->unsignedInteger('expense_amount')->nullable();
            $table->string('expense_desc')->nullable();
            $table->timestamps();

            $table->foreign('expense_category_id')
              ->references('id')->on('expense_categories');
            $table->foreign('day_sheet_id')
                  ->references('id')->on('day_sheets')
                  ->onDelete('cascade');
            $table->foreign('user_id')
                  ->references('id')->on('users')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('daily_expense_transactions');
    }
}
