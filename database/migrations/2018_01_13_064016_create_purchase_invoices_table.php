<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->nullable();
            $table->unsignedInteger('chem_type_id')->nullable();
            $table->unsignedInteger('monthly_cost_of_sale_id')->nullable();
            $table->unsignedInteger('chem_sale_price_id')->nullable();

            $table->date('invoice_date')->nullable();
            $table->string('invoice_no')->nullable();
            $table->unsignedInteger('invoice_quantity')->nullable();
            $table->unsignedInteger('invoice_amount')->nullable();
            $table->string('invoice_ref_no')->nullable();
            $table->string('invoice_field1')->nullable();
            $table->unsignedInteger('invoice_profit_margin')->nullable();
            $table->unsignedInteger('invoice_price_per_unit')->nullable();
            $table->timestamps();

            $table->foreign('user_id')
                  ->references('id')->on('users')
                  ->onDelete('cascade');
            $table->foreign('chem_type_id')
                  ->references('id')->on('chem_types');
            $table->foreign('monthly_cost_of_sale_id')
                  ->references('id')->on('monthly_cost_of_sales');
            $table->foreign('chem_sale_price_id')
                  ->references('id')->on('chem_sale_prices');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_invoices');
    }
}
