<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMonthlyProfitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('monthly_profits', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->nullable();
            $table->date('month_year')->nullable();
            $table->unsignedInteger('monthly_gross_revenue')->nullable();
            $table->unsignedInteger('monthly_total_cost_of_sale')->nullable();
            $table->unsignedInteger('monthly_total_expenses')->nullable();
            $table->unsignedInteger('monthly_net_profit')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('monthly_profits');
    }
}
