<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDailySalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('daily_sales', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->nullable();
            $table->unsignedInteger('day_sheet_id')->nullable();
            $table->unsignedInteger('chem_type_id')->nullable();
            $table->unsignedInteger('chem_sale_price_id')->nullable();
            $table->unsignedInteger('daily_quantity_sold')->nullable();
            $table->unsignedInteger('daily_revenue_sales')->nullable();

            $table->timestamps();

            $table->foreign('user_id')
                  ->references('id')->on('users')
                  ->onDelete('cascade');
            $table->foreign('chem_type_id')
                  ->references('id')->on('chem_types');
            $table->foreign('chem_sale_price_id')
                  ->references('id')->on('chem_sale_prices');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('daily_sales');
    }
}
