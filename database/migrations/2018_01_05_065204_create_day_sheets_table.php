<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDaySheetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('day_sheets', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->nullable();
            $table->dateTime('daysheet_date')->nullable();
            $table->unsignedInteger('monthly_cost_of_sale_id')->nullable();
            $table->unsignedInteger('daily_expenses_total')->nullable();
            $table->unsignedInteger('daily_sales_total')->nullable();
            $table->unsignedInteger('daily_revenue_total')->nullable();
            $table->timestamps();

            $table->foreign('user_id')
                  ->references('id')->on('users')
                  ->onDelete('cascade');
            $table->foreign('monthly_cost_of_sale_id')
                  ->references('id')->on('monthly_cost_of_sales');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('day_sheets');
    }
}
