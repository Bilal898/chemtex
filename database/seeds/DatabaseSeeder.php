<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::statement("SET foreign_key_checks = 0");
      // DB::table('expense_categories')->delete();
      // DB::table('expense_transactions')->delete();
      DB::table('users')->truncate();
      // DB::table('quotes')->truncate();



      $this->call(UsersTableSeeder::class);
      // $this->call(ExpenseCategoryTableSeeder::class);
      // $this->call(ExpenseTransactionTableSeeder::class);
      // $this->call(DaySheetTableSeeder::class);
      DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
